import java.awt.Color;
import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

/**
 * Classe principale permettant d'illustrer l'ajout d'une instance d'un composant d'animation.
 * 
 */


public class Application extends JFrame {
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private ZoneAnimation zoneAnim;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Application frame = new Application();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public Application() {
		setTitle("Animation");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 359, 297);
		contentPane = new JPanel();
		contentPane.setBackground(Color.CYAN);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		zoneAnim = new ZoneAnimation();
		zoneAnim.setBounds(36, 55, 266, 169);
		contentPane.add(zoneAnim);
		
		// pour commencer on devra faire zoneAnim.demarerr() 
		
		
	}//fin consctructeur
	
}//fin classe
