
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JPanel;

public class ZoneAnimation extends JPanel implements Runnable {
	private static final long serialVersionUID = 1L;
	private boolean premiereFois = true; //optionnel (voir paintComponent)
	private boolean enCoursDAnimation = false;
	
	/**
	 * Constructeur
	 */

	public ZoneAnimation() {
		
		setBackground(Color.yellow);
		
		/* Exemples de code qu'on y retrouvera:
		 * 
		 * -- couleur du fond du composant
		 * -- initialisation de certains champs
		 * -- lecture des images
		 * -- attention, aucun getWidth ni getHeight!
		 */
		
	}

	/**
	 * Dessiner le contenu du composant graphique
	 * @param g Le contexte graphique
	 */
	
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
	
		if (premiereFois) {
			/*
			 * Section optionnelle
			 * --on y placerais certaines instructions � ne faire qu'une seule fois, mais qui n�cessitent d'appeler getWidth ou getHeight!!
			 */
			premiereFois = false;
		}

		//On place ici les instructions de dessin (g2d.setColor, g2d.fill, g2d.draw....etc )
	}

	/**
	 * Gerer le processus (thread)
	 * A chaque iteration, modifie les variables, fait une demande de dessin, puis fait une pause.
	 */
	
	@Override
	public void run() {
		while (enCoursDAnimation) { 
			
			//ajustement des variables qui gerent l'animation (tout ce qui change d'une image � l'autre: positions, couleurs, dimensions etc etc)
			
			//demande de mise a jour
			repaint();
			
			//pause obligatoire
			try {
				Thread.sleep(50); //on regle la valeur du sleep pour obtenir un rythme agr�able
			}
			catch (InterruptedException e) {
				System.out.println("run : processus interrompu!"); 
			}
			
		}//fin while
		
		System.out.println("L'animation est termin�e...");
		
	} // fin run
	
	/**
	 * Creer et demarrer le processus d'animation.
	 */
	public void demarrer() {
		if (!enCoursDAnimation) { 
			Thread processusAnim = new Thread(this);
			processusAnim.start();
			enCoursDAnimation = true;
		}
	}//fin methode
	
	
	/**
	 * Arreter le processus d'animation.
	 * Causera la fin de la boucle dans la methode run, ce qui mettra fin au processus.
	 */
	public void arreter() {
		enCoursDAnimation = false;
	}//fin methode

	
}//fin classe

















